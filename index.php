<?php

use Acupuncture\Utils\Router;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config/routerConfig.php';

$db = \Acupuncture\Db\DbConnection::getConnection();
$router = new Router(array('base_url' => 'http://acpt.tk/'));

configRouter($router, $db);

try {
    $router->run();
} catch(Exception $e) {
    $ctrl = new \Acupuncture\Controllers\NotFoundController($db);
    $ctrl->run($router);
}