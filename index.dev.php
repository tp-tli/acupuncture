<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config/debugConfig.php';
require __DIR__ . '/config/routerConfig.php';

use Acupuncture\Db\DbConnection;
use Acupuncture\Utils\Router;

$db = DbConnection::getConnection();
$router = new Router(array('base_url' => 'http://localhost:8000/index.dev.php'));

configRouter($router, $db);

try {
    $router->run();
} catch(Exception $e) {
    throw $e;
}