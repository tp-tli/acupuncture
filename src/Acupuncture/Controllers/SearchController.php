<?php
namespace Acupuncture\Controllers;

class SearchController extends SecureController {

    public function subRender(\Acupuncture\Utils\Router $router, $params = array()) {
        $this->render('search', $params);
    }
}