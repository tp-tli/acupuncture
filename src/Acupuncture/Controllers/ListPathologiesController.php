<?php

namespace Acupuncture\Controllers;


use Acupuncture\Models\Pathology;

class ListPathologiesController extends SecureController {

    public function subRender(\Acupuncture\Utils\Router $router, $params = array()) {
        $maxPages = $this->getMaxPages();
        if (isset($_GET['page'])) {
            $page = intval($_GET['page']);
            if ($page >= $maxPages) {
                $page = $maxPages;
            } elseif($page < 1) {
                $page = 1;
            }
        } else {
            $page = 1;
        }
        $pathology = new Pathology($this->db);
        $rows = $pathology->selectAll($page);

        $params['maxPages'] = $maxPages;
        $params['rows'] = $rows;

        $this->render('list-pathologies', $params);
    }

    private function getMaxPages($limit = 50) {
        $pathology = new Pathology($this->db);
        $nb = $pathology->count();
        $max = intval($nb / $limit) + 1;
        return $max;
    }
}
