<?php
namespace Acupuncture\Controllers;

use Acupuncture\Models\PathologySymptom;

class DisplayPathologyController extends SecureController {

    public function subRender(\Acupuncture\Utils\Router $router, $params = array()) {
        $model = new PathologySymptom($this->db);
        $get = array (
            'name' => isset($_GET['name']) ? $_GET['name'] : '',
            'symptoms' => isset($_GET['symp']) ? $_GET['symp'] : '',
            'meridians' => isset($_GET['mer']) ? $_GET['mer'] : '',
        );

        $params['rows'] = $model->select($get);
        $this->render('display-info', $params);
    }
}