<?php
namespace Acupuncture\Controllers;

use Vinelab\Rss\Rss;
use Acupuncture\Utils\Session;

class IndexController extends AbstractRouteController {

    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        $rss = new Rss();
        $feed = $rss->feed('http://korben.info/feed')->articles();
        $articles = array();
        foreach($feed as $article) {
            $articles[] = array(
                'title' => $article->title,
                'link' => $article->link,
                'description' => $article->description,
                'date' => $article->lastBuiltDate,
            );
        }
        Session::getOrCreate();
        $this->render('index', array(
            'params' => $params,
             'articles' => $articles,
             'authenticate' => isset($_SESSION['username'])
             ));
    }
}