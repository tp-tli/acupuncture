<?php
namespace Acupuncture\Controllers;

class CreateUserController extends AbstractRouteController {

    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        $this->render('create-user');
    }
}