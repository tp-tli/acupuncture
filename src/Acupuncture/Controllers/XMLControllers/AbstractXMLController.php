<?php

namespace Acupuncture\Controllers\XML;


use Acupuncture\Controllers\AbstractRouteController;
use SimpleXMLElement;

abstract class AbstractXMLController extends AbstractRouteController {

    public function renderXml(\DOMDocument $object) {
        header('Content-Type: application/xml; charset=UTF-8');
        print $object->saveXML();
    }

}