<?php

namespace Acupuncture\Controllers\XML;


use Acupuncture\Models\PathologySymptom;
use Acupuncture\Utils\Router;

class XMLListSymptomsController extends AbstractXMLController {

    public function run(Router $router, $params = array()) {
        $page = 1;
        if (isset($params['page']) && intval($params['page']) !== 0) {
            $page = intval($params['page']);
        }
        $model = new PathologySymptom($this->db);
        $rows = $model->selectAll($page);

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $XMLPathologies = $xml->createElementNS(
            'http://'. $_SERVER['SERVER_NAME'] . '/web/xsd/pathologies.xsd',
            'pathologies'
        );
        $XMLPathologies->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $XMLPathologies->setAttributeNS(
                'http://www.w3.org/2001/XMLSchema-instance',
                'schemaLocation',
                'http://' . $_SERVER['SERVER_NAME'] . '/web/xsd/pathologies.xsd'
        );

        foreach ($rows as $pathology => $row) {
            $XMLSymptoms = $xml->createElement('symptoms');
            foreach ($row['symptoms'] as $symptom) {
                $XMLSymptom = $xml->appendChild(
                    $xml->createElement('symptom', $symptom)
                );
                $XMLSymptoms->appendChild($XMLSymptom);
            }

            $XMLpathology = $xml->createElement('pathology');
            $XMLpathology->appendChild(
                $xml->createElement('meridian', $row['meridian'])
            );
            $XMLpathology->appendChild(
                $xml->createElement('type', $row['type'])
            );
            $XMLpathology->appendChild($XMLSymptoms);

            $XMLPathologies->appendChild($XMLpathology);
        }
        $xml->appendChild($XMLPathologies);
        $this->renderXml($xml);
    }
}