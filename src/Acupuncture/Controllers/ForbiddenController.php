<?php
namespace Acupuncture\Controllers;

class ForbiddenController extends AbstractRouteController {

    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        $this->render('forbidden');
        http_response_code(403);
    }
}