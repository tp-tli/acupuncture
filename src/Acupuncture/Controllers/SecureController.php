<?php
namespace Acupuncture\Controllers;

use Acupuncture\Utils\Session;
use Acupuncture\Utils\Router;

abstract class SecureController extends AbstractRouteController {
    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        Session::getOrCreate();
        if(isset($_SESSION['username'])) {
            $this->subRender($router, array('authenticate' => true));
        } else {
            $router->redirectTo('get_error');
        }
    }
}