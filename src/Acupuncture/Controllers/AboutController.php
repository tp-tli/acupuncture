<?php
namespace Acupuncture\Controllers;

class AboutController extends SecureController {
    public function subRender(\Acupuncture\Utils\Router $router, $params = array()) {
        $this->render('about', $params);
    }
}