<?php

namespace Acupuncture\Controllers\JSON;


use Acupuncture\Controllers\AbstractRouteController;

abstract class AbstractJSONController extends AbstractRouteController {

    public function renderJSON($object) {
        header('Content-Type: application/json');
        print json_encode($object);
    }

}