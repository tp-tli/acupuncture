<?php

namespace Acupuncture\Controllers\JSON;


use Acupuncture\Models\PathologySymptom;
use Acupuncture\Utils\Router;

class JSONListSymptomsController extends AbstractJSONController {

    public function run(Router $router, $params = array()) {
        $page = 1;
        if (isset($params['page']) && intval($params['page']) !== 0) {
            $page = intval($params['page']);
        }

        $model = new PathologySymptom($this->db);
        $rows = $model->selectAll($page);


        $json = array();
        foreach ($rows as $pathology => $row) {
            $pathologyArray = array(
                'pathology' => $pathology,
            );
            $json[] = array_merge_recursive($pathologyArray, $row);
        }

        $this->renderJSON($json);
    }
}