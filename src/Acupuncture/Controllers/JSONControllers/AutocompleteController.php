<?php
namespace Acupuncture\Controllers\JSON;

use Acupuncture\Models\Filterable;

class AutocompleteController extends AbstractJSONController {

    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        if (!isset($this->params['model'])) {
            throw new \Exception('you should provide a model class parameter for an AutocompleteController');
        }
        $model = $this->params['model'];
        $pathology = new $model($this->db);
        if (!$pathology instanceof Filterable) {
            throw new \Exception('the provided model is not a Filterable');
        }
        $rows = $pathology->filter(html_entity_decode($_GET['q']));
        $this->renderJSON($rows);
    }
}
