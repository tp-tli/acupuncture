<?php
namespace Acupuncture\Controllers;

use Acupuncture\Utils\Session;
use Acupuncture\Models\User;

class DisconnectController extends AbstractRouteController {
    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        Session::getOrCreate();
        session_destroy();
        $router->redirectTo('get_index');
    }
}