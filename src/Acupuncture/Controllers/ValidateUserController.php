<?php
namespace Acupuncture\Controllers;

use Acupuncture\Models\User;

class ValidateUserController extends AbstractRouteController {

    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        if(isset($_POST['password1']) && isset($_POST['password2'])
            && $_POST['password1'] == $_POST['password2']) {
            // Test the value of the username and password
            $password = $_POST['password1'];
            $username = $_POST['username'];
            $user = new User($this->db);
            $success = $user->createUser(
                $username,
                $this->encryptPassword($password)
            );
            if ($success == true) {
                $this->render('success');
            } else {
                $this->render('error');
            }
        } else {
            $this->render('error');
        }
    }

    private function encryptPassword($password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}