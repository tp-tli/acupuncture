<?php
namespace Acupuncture\Controllers;

use Acupuncture\Config\TwigConfig;
use Acupuncture\Utils\Router;
use PDO;

abstract class AbstractRouteController {
    protected $db;
    protected $params;
    public function __construct(PDO $pdo, $params = array()) {
        $this->db = $pdo;
        $this->params = $params;
    }

    public function render($templateName, $params = array()) {
        $twig = TwigConfig::getTwig();
        $twig->display("{$templateName}.tpl.html", $params);
    }

    abstract public function run(Router $router, $params = array());
}
