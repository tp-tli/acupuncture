<?php
namespace Acupuncture\Controllers;

use Acupuncture\Utils\Session;
use Acupuncture\Models\User;

class AuthenticationUserController extends AbstractRouteController {
    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        Session::getOrCreate();
        if(isset($_SESSION[$_POST['username']])) {
            $router->redirectTo('get_index');
        }
        else if ($this->verificationLoginPass($_POST['username'], $_POST['password'])) {
            $_SESSION['username'] = $_POST['username'];
            $router->redirectTo('get_about');
        } else {
            $router->redirectTo('get_error');
        }
    }

    private function verificationLoginPass($username, $password) { 
        // Test the value of the username and password
        $user = new User($this->db);
        $success = $user->authenticateUser(
            $username,
            $password
        );
        return $success;
    }

}