<?php

namespace Acupuncture\Controllers;


use Acupuncture\Models\PathologySymptom;
use Acupuncture\Utils\Router;

class ListSymptomsController extends SecureController {

    public function subRender(Router $router, $params = array()) {

        $page = 1;
        if (isset($params['page']) && intval($params['page']) !== 0) {
            $page = intval($params['page']);
        }

        $model = new PathologySymptom($this->db);
        $rows = $model->selectAll($page);

        $params['rows'] = $rows;
        $params['nextPage'] = $page + 1;
        $params['previousPage'] = $page - 1;

        $this->render('list-pathologies-and-symptoms', $params);
    }
}