<?php
namespace Acupuncture\Controllers;

class NotFoundController extends AbstractRouteController {

    public function run(\Acupuncture\Utils\Router $router, $params = array()) {
        http_response_code(404);
        $this->render('not-found', array('params' => $params));
    }
}