<?php
namespace Acupuncture\Config;

use Acupuncture\Utils\Router;
use Twig_Environment;
use Twig_Extension_Debug;
use Twig_Function;
use Twig_Loader_Filesystem;

class TwigConfig
{
    private static $twig;
    public static function configTwig(Router $router) {
        if (isset(TwigConfig::$twig)) {
            return TwigConfig::$twig;
        }

        $opts = array();
        $opts['debug'] = !(isset($_ENV['PHP_ENV']) && $_ENV['PHP_ENV'] === 'production');

        $loader = new Twig_Loader_Filesystem('views');
        $twig = new Twig_Environment($loader, $opts);
        $twig->addExtension(new Twig_Extension_Debug());
        $function = new Twig_Function('routeTo', function($routeName, $routeParams = array()) use ($router) {
            return $router->getRouteTo($routeName, $routeParams);
        });
        $twig->addFunction($function);

        TwigConfig::$twig = $twig;
    }

    public static function getTwig() {
        if (!isset(TwigConfig::$twig)) {
            throw new \Exception('twig is not configured');
        }

        return TwigConfig::$twig;
    }

}

