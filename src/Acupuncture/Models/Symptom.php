<?php

namespace Acupuncture\Models;

class Symptom extends AbstractDao implements Filterable {

    public function filter($query) {
        $sth = $this->pdo->prepare(
            'SELECT idS, symptome.desc FROM symptome WHERE symptome.desc LIKE :query'
        );
        $sth->bindValue(':query', "%$query%");
        $sth->execute();
        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }
}
