<?php

namespace Acupuncture\Models;

interface Filterable {
    public function filter($query);
}
