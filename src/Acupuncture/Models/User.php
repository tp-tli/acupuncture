<?php

namespace Acupuncture\Models;


class User extends AbstractDao
{
    public function createUser($username, $password) {
        $sth = $this->pdo->prepare("INSERT INTO user(username, password) VALUES (:username, :password);");
        return $sth->execute(array(
            ':username' => $username,
            ':password' => $password
        ));
    }

    public function authenticateUser($username, $password) {
        $sth = $this->pdo->prepare("SELECT password FROM `user` WHERE username=:username");
        $sth->execute(array(
            ':username' => $username
        ));
        $row = $sth->fetch();
        return password_verify($password, $row[0]);
    }
}