<?php

namespace Acupuncture\Models;

use PDO;

class Pathology extends AbstractDao implements Filterable {

    public function selectAll($page = 1, $limit = AbstractDao::LIMIT) {
        $sth = $this->pdo->prepare("SELECT idP, patho.desc FROM patho LIMIT :limit OFFSET :offset");
        $sth->bindParam(':limit', $limit, PDO::PARAM_INT);
        $offset = ($page - 1) * $limit;
        $sth->bindParam(':offset', $offset, PDO::PARAM_INT);
        $sth->execute();
        $rows = $sth->fetchAll();
        return $rows;
    }

    public function filter($query) {
        $sth = $this->pdo->prepare(
            "SELECT idP, patho.desc FROM patho WHERE patho.desc LIKE :query"
        );
        $sth->bindValue(':query', "%$query%");
        $sth->execute();
        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function count() {
        $query = "SELECT COUNT(idP) as count FROM patho";
        $r = $this->pdo->query($query)->fetch();
        return intval($r[0]);
    }
}
