<?php

namespace Acupuncture\Models;

use PDO;

abstract class AbstractDao {
    protected $pdo;
    const LIMIT = 50;
    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }
}