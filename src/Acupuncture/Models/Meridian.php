<?php

namespace Acupuncture\Models;

class Meridian extends AbstractDao implements Filterable {

    public function filter($query) {
        $sth = $this->pdo->prepare(
            'SELECT code, nom FROM meridien WHERE nom LIKE :query'
        );
        $sth->bindValue(':query', "%$query%");
        $sth->execute();
        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }
}
