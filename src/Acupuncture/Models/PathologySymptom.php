<?php

namespace Acupuncture\Models;


use PDO;

class PathologySymptom extends AbstractDao {

    private function decryptType($type) {
        /*
         * Ici on ne suit que les valeurs qui sont fournies dans l'énoncé
         * meme si la base en contient différentes variations
         */
        $types = array(
            'm' => array('value' => 'méridien',
                        'children' => array(
                            'i' => (array('value' => 'interne')),
                            'e' => (array('value' => 'externe'))
                        )),
            'tf' => array('value' => 'tsang/fu',
                        'children' => array(
                            'p' => array('value' => 'plein'),
                            'v' => array('value' => 'vide'),
                            'c' => array('value' => 'chaud'),
                            'f' => array('value' => 'froid')
                        )),
            'j' => array('value' => 'jing jin'),
            'l' => array('value' => 'voies luo',
                    'children' => array(
                        'v' => array('value' => 'vide'),
                        'p' => array('value' => 'plein')
                    )),
            'mv' => array('value' => 'merveilleux vaisseaux')
        );
        $re = '#(m|tf|j|l|mv)((?:(?!i$)i|e|p|v|c|f)*)(m|s|i)?$#';
        if (preg_match($re, $type, $matches)) {
            $content = $types[$matches[1]]['value'];
            if (isset($matches[2]) && $matches[2] !== '') {
                foreach(str_split($matches[2]) as $char) {
                    if (isset($types[$matches[1]]['children'][$char]['value'])) {
                        $content .= ' ' . $types[$matches[1]]['children'][$char]['value'];
                    } else {
                        return $type;
                    }
                }
            }

            if (isset($matches[3]) && $matches[3] !== '') {
                $modifiers = array(
                    'i' => array('value' => 'inférieur'),
                    'm' => array('value' => 'moyen'),
                    's' => array('value' => 'supérieur'),
                );

                if (!isset($modifiers[$matches[3]]['value'])) {
                     return $type;
                }

                $content .= ' ' . $modifiers[$matches[3]]['value'];
            }
            return $content;
        }

        return $type;
    }

    public function selectAll($page = 1, $limit = AbstractDao::LIMIT) {

        $sql = <<<EOF
SELECT m.nom meridian,
       p.type pathology_type,
       p.desc as pathology,
       s.desc as symptom
FROM patho as p
    JOIN symptPatho as sp on sp.idP = p.idP
    JOIN symptome as s on s.idS = sp.idS
    JOIN meridien as m on m.code = p.mer
INNER JOIN (
    SELECT p2.desc as p2d
    FROM patho p2
    ORDER BY p2d ASC
    LIMIT :limit
    OFFSET :offset) as p2
    ON p2.p2d = p.desc
ORDER BY pathology ASC;
EOF;
        $offset = ($page - 1) * $limit;
        $sth = $this->pdo->prepare($sql);

        $sth->bindParam(':limit', $limit, PDO::PARAM_INT);
        $sth->bindParam(':offset', $offset, PDO::PARAM_INT);
        $sth->execute();
        $rows = $sth->fetchAll();

        $pathologies = array();
        foreach($rows as $row) {
            $pathologies[$row['pathology']]['symptoms'][] = $row['symptom'];
            $pathologies[$row['pathology']]['meridian'] = $row['meridian'];
            $pathologies[$row['pathology']]['type'] = $this->decryptType($row['pathology_type']);
        }

        return $pathologies;
    }

    private function CSVToStatement($column, $csv) {
        $fields = array_map(
            function () { return "?"; },
            array_unique(
                explode(',', $csv)
            )
        );

        return "$column LIKE " . join(" OR $column LIKE ", $fields);
    }

    public function select($query, $limit = AbstractDao::LIMIT) {

        $patho = $query['name'];
        $meridian = $query['meridians'];
        $symptom = $query['symptoms'];

        $sql = <<<EOF
SELECT m.nom meridian,
       p.type pathology_type,
       p.desc as pathology,
       s.desc as symptom
FROM patho as p
    JOIN symptPatho as sp on sp.idP = p.idP
    JOIN symptome as s on s.idS = sp.idS
    JOIN meridien as m on m.code = p.mer
INNER JOIN (
    SELECT p2.desc as p2d
    FROM patho p2
    WHERE p2.desc LIKE ?) as p2
    ON p2.p2d = p.desc
EOF;

        $sql .= ' WHERE ' . $this->CSVToStatement('m.nom', $meridian);
        $sql .= ' AND ' . $this->CSVToStatement('s.desc', $symptom);

        $sth = $this->pdo->prepare($sql);
        $sth->bindValue(1, '%' . $patho . '%', PDO::PARAM_STR);

        $i = 2;
        foreach (array($meridian, $symptom) as $query) {
            if ($query === '') {
                $sth->bindValue($i, '%%', PDO::PARAM_STR);
                $i++;
            } else {
                foreach (array_unique(explode(',', $query)) as $idx => $q) {
                    $sth->bindValue($i, '%' . $q . '%', PDO::PARAM_STR);
                    $i++;
                }
            }
        }
        $sth->execute();
        $rows = $sth->fetchAll();

        $pathologies = array();
        foreach($rows as $row) {
            $pathologies[$row['pathology']]['symptoms'][] = $row['symptom'];
            $pathologies[$row['pathology']]['meridian'] = $row['meridian'];
            $pathologies[$row['pathology']]['type'] = $this->decryptType($row['pathology_type']);
        }
        return $pathologies;
    }
}