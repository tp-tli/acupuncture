<?php
namespace Acupuncture\Db;

use PDO;

class DbConnection
{
    public static function getConnection()
    {
        $db = new PDO('mysql:host=localhost;dbname=acu;port=3306;charset=UTF8', 'acu_u', 'pwd_acu');
        return $db;
    }
}

