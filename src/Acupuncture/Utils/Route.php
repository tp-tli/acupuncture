<?php

namespace Acupuncture\Utils;

use Acupuncture\Controllers\AbstractRouteController;
use Exception;

/**
 * Class Route, inspired by https://grafikart.fr/tutoriels/php/router-628
 * @package Acupuncture\Utils
 */
class Route {
    private $name;
    private $url;
    private $controller;
    private $matches;
    private $params;
    private $order;

    public function __construct($params) {
        $url = $params['url'];
        $name = $params['name'];
        $controller = $params['controller'];

        if (!$controller instanceof AbstractRouteController) {
            throw new \Exception('You should provide a controller instanceof AbstractRouteController');
        }

        preg_match_all('#:([\w-]+)#', $url, $matches);
        $i = 0;
        if (isset($matches)) {
            array_shift($matches);
            foreach ($matches[0] as $match) {
                $this->matches[$match] = '';
                $this->order[$i++] = $match;
            }
        }

        $this->name = $name;
        $this->url = preg_replace('#:[\w-]+#', '([^/]+)', trim($url, '/'));
        $this->controller = $controller;
        $this->params = $params;
    }

    /**
     * @param $name
     * @return bool true if the name of the route is $name
     */
    public function is($name) {
        return $this->name === $name;
    }

    private function buildUrl($params = array()) {
        $url = $this->url;
        $i = 0;
        while(isset($this->matches)
                && isset($this->order[$i])
                && isset($params[$this->order[$i]])) {
            $param = $params[$this->order[$i]];
            $url = preg_replace('#\(\[\^\/\]\+\)#', $param, $url, 1);
            ++$i;
        }

        return $url;
    }

    /**
     * getter to the url attribute
     * @param array $params the list of params to build together with the url
     * @return string the url attribute
     */
    public function url($params = array()) {
        return $this->buildUrl($params);
    }

    /**
     * @param $url
     * @return bool true if the $url matches the $this->url with params
     */
    public function match($url) {
        $url = trim($url, '/');
        if (!preg_match("#^{$this->url}$#", $url, $matches)) {
            return false;
        }

        $i = 1;
        if (isset($this->matches)) {
            foreach (array_keys($this->matches) as $key) {
                if (isset($matches[$i])) {
                    $this->matches[$key] = $matches[$i++];
                }
            }
        }

        return true;
    }

    /**
     * run the controller run method
     * @return mixed
     */
    public function run(\Acupuncture\Utils\Router $router) {
        return $this->controller->run($router, $this->matches);
    }

    public function toString() {
        return "{ name: {$this->name}, url: {$this->url}, controller: " . get_class($this->controller) . " }";
    }
}