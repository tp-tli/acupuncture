<?php

namespace Acupuncture\Utils;

use Acupuncture\Controllers\AbstractRouteController;
use Exception;

/**
 * Class Router, inspired by https://grafikart.fr/tutoriels/php/router-628
 * @property string defaultRoute
 * @package Acupuncture\Utils
 */
class Router
{
    private $routes = array();
    private $defaultRoute;
    private $baseUrl = '';
    public function __construct($params = array()) {
        if (isset($params['base_url'])) {
            $this->baseUrl = rtrim($params['base_url'], '/');
        }

    }

    private $HTTP_METHODS = array(
        'GET',
        'POST',
        'PUT',
        'PATCH'
    );

    public function baseUrl() {
        return $this->baseUrl;
    }

    /**
     * @param string $methodName the method to use, must be one of $HTTP_METHODS
     * @param array $routeParams an array with `controller`, `url` and `name` as route params. `controller`
     * should be an instance of AbstractRouteController
     * @throws Exception
     * @return $this
     */
    public function method($methodName, array $routeParams) {
        $route = $routeParams['url'];
        $name = $routeParams['name'];
        $controller = $routeParams['controller'];

        if (!$controller instanceof AbstractRouteController) {
            throw new Exception('You should use an instanceof AbstractRouteController as controller');
        }

        if (!in_array($methodName, $this->HTTP_METHODS)) {
            throw new Exception('You should provide a valid HTTP method');
        }

        $this->routes[$methodName][] = new Route(array('url' => $route, 'name' => $name, 'controller' => $controller));
        return $this;
    }

    /**
     * alias of this->method('GET',...
     * @param array $routeParams
     * @see __construct
     * @return $this
     */
    public function get(array $routeParams) {
        return $this->method('GET', $routeParams);
    }

    /**
     * alias of this->method('POST',...
     * @param array $routeParams
     * @return $this
     */
    public function post(array $routeParams) {
        return $this->method('POST', $routeParams);
    }

    /**
     * @param string $routeName the name of the route to find in the array of routes
     * @return bool if the routename exists in the array of routes
     */
    private function routeExists($routeName) {
        foreach ($this->routes as $methodRoute) {
            foreach ($methodRoute as $route) {
                if ($route->is($routeName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * find the Route instance by its name
     * @param string $routeName
     * @return Route $route
     * @throws Exception
     * @see Route::is
     */
    public function route($routeName) {
        foreach($this->routes as $methodRoute) {
            foreach ($methodRoute as $route) {
                if ($route->is($routeName)) {
                    return $route;
                }
            }
        }

        throw new Exception("Impossible to retrieve route from route name $routeName");
    }

    /**
     * return the string url to a specific name Route.
     * @param string $routeName
     * @param array $params to generate the correct URL
     * @return string
     */
    public function getRouteTo($routeName, $params = array()) {
        if (!$this->routeExists($routeName)) {
            throw new \Exception("Unknown route $routeName");
        }

        $togo = $this->baseUrl;
        $togo .= isset($_SERVER['URL_REWRITING']) ? '/' : '/?route=';
        $route = $this->route($routeName);
        $togo .= $route->url($params);
        return $togo;
    }

    /**
     * helper to header('location: ...'), using the routes in the routes array
     * @param $routeName
     * @throws Exception if the routeName does not corresponds to any route
     */
    public function redirectTo($routeName) {
        if (!$this->routeExists($routeName)) {
            throw new Exception("Impossible to redirect to unknown route $routeName");
        }

        $location = $this->getRouteTo($routeName);


        header("Location: $location");
    }

    /**
     * @throws Exception if the provided route does not match
     * @return mixed
     */
    public function run() {
        if (isset($_GET['route']) && !empty($_GET['route'])) {
            $url = $_GET['route'];
        } else {
            $this->redirectTo($this->defaultRoute);
            return;
        }

        $method = $_SERVER['REQUEST_METHOD'];

        if (isset($this->routes[$method])) {
            foreach($this->routes[$method] as $route) {
                if ($route->match($url)) {
                    return $route->run($this);
                }
            }
        }

        throw new Exception('No route found!');
    }

    public function defaultRoute($routeName = '') {
        if ((!isset($routeName) || $routeName === '') && isset($this->defaultRoute)) {
            return $this->defaultRoute;
        }

        if (!$this->routeExists($routeName)) {
            throw new \Exception('You tried to define a default route that does not exist');
        }


        $this->defaultRoute = $routeName;
    }

    public function toString() {
        $str = "Router: {\n";
        foreach ($this->routes as $method => $methodRoutes) {
            $str .= "  $method: [\n";
            foreach($methodRoutes as $route) {
                $str.= "    " . $route->toString() . "\n";
            }
            $str .= "  ]\n";
        }
        $str .= "}";
        return $str;
    }
}