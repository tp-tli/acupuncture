<?php

namespace Acupuncture\Utils;

/*
    Use the static method getInstance to get the object.
*/

class Session
{
    public static function getOrCreate(){
        if(!isset($_SESSION)) {
            session_start();
        }
    }
}