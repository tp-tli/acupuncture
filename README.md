# [acpt.tk](http://acpt.tk)

## Membres de l'équipe

+ Constant Deschietere (DC) : majeure info
+ Quentin Fleury (FQ) : majeure réseaux
+ Mike Flory-Cellini (FM) : majeure réseaux
+ Jordan Quagliatini (QJ) : majeure info

## Répartition des tâches

| Tâches                                  | effort (sur 5) | Attribué à |
|-----------------------------------------|----------------|------------|
| Développement du routeur                | 4              | QJ         |
| Architecture générale                   | 2              | QJ         |
|                                         | 2              | DC         |
| Sécurisation                            | 2              | DC         |
| Modèle d'authentification               | 5              | FM         |
| style                                   | 3              | DC         |
| accessibilité                           | 1              | QJ         |
|                                         | 1              | FM         |
| Requêtes SQL                            | 3              | DC         |
| parsing des types de pathologies        | 2              | QJ         |
| automatisation de la mise en production | 5              | FQ         |
| gestion des serveurs de production      | 3              | FQ         |
|                                         | 2              | FM         |
| script d'autocomplétion                 | 3              | DC         |
|                                         | 3              | QJ         |
| Finitions                               | 2              | QJ         |

Ce qui nous donne le tableau suivant

| Personne | Total effort | Total effort (%) |
|----------|--------------|------------------|
| QJ       | 15           | 34,09 %          |
| DC       | 13           | 29,55 %          |
| FM       | 8            | 18,18 %          |
| FQ       | 8            | 18,18 %          |


## Description

Une version en ligne est disponible sur [](http://acpt.tk)

### Note sur la gestion des utilisateurs

Chaque utilisateur est enregistré en base en utilisant un système
de hachage et salage des mot de passe: `password_hash` avec la 
fonction de hashage: `bcrypt`.

### GET / GET /home

Affiche une page d'accueil avec un flux RSS (provenant de [](https://korben.info))

### POST /login

Gère la connexion d'un utilisateur en vérifiant que l'utilisateur peut se connecter

### GET /error

La page d'erreur, cette page s'affiche en cas de problème pendant la communication
avec le serveur.

### GET /create-user

Affiche un formulaire pour créer un utilisateur

### POST /create-user

Gère la soumission du formulaire de création d'un nouvel utilisateur.
Lorsque l'utilisateur est créé avec succès on est redirigé ver la page about

> **[NOTE]**: L'ensemble des autres pages nécessitent d'être connecté pour être affiché,
    on peut créer un utilisateur en cliquant sur le lien "Pas de compte ?" sur le bandeau.
    L'utilisateur par défaut est jojo (mot de passe : `jojo`).
    Les routes REST ne nécessitent pas cette connexion.

### GET /pathologies

Affiche une table des pathologies disponibles sur le site

### GET /symptoms-and-pathologies

Affiche un tableau de pathologies avec leur méridien, symptômes et type associés.

### GET /search

Affiche un formulaire de recherche de pathologie autocomplété.

Il est possible de rajouter des critères de recherche en ajoutant un méridien ou
des symptômes.
Il est également possible de rechercher, par exemple, toutes les pathologies
ayant un méridien ou des symptômes particuliers, en ne saisissant que ces champs,
eux-mêmes autocomplétés.

### GET /display?name=[name]

Affiche la fiche d'une pathologie en particulier. Particulièrement
utile lorsqu'utilisée avec le champ de recherche.

### GET /about

Affiche une page d'information générale sur le site.

### GET /api/xml/pathologies

Affiche 50 pathologies avec leur catégorie et symptômes.

+ Content-Type: `application/xml`
```xml
<pathologies xsi:schemaLocation="http://acpt.tk/web/xsd/pathologies.xsd">
    <pathology>
        <meridian>Triple réchauffeur</meridian>
        <type>tsang/fu plein chaud inférieur</type>
        <symptoms>
            <symptom>Défécation et miction difficiles</symptom>
            <symptom>Les selles peuvent contenir du sang et du pus</symptom>
        </symptoms>
    </pathology>
    <pathology>
        ...
    </pathology>
</pathologies>
```
Il est possible de paginer le résultat en utilisant un paramètre page à la
suite de l'url.

```
/api/xml/pathologies/page/:page
/api/xml/pathologies/page/3
```

### GET /api/json/pathologies

Affiche 50 pathologies avec leur catégorie, symptômes et méridien.

+ Content-Type: `application/json`
```json
[

    {
        "pathology": "fu du triple réchauffeur inférieur plein et chaud",
        "symptoms": [
            "Défécation et miction difficiles",
            "Les selles peuvent contenir du sang et du pus"
        ],
        "meridian": "Triple réchauffeur",
        "type": "tsang/fu plein chaud inférieur"
    },
    {
      "pathology": ...
    }
]
```

Comme pour la route en XML il est possible de paginer les résultats.

### GET /api/json/autocomplete/pathologies?q=[query]

Effectue l'autocomplétion d'une recherche de pathologie.

+ ?q=poumon
+ Content-Type: application/json

```json
[
    ...
    {
        "idP": "2",
        "desc": "méridien du poumon interne"
    },
    ...
]
```

### GET /api/json/autocomplete/meridians?q=[query]

Effectue l'autocomplétion d'une recherche de méridien.

+ ?q=foie
+ Content-Type: application/json

```json
[

    {
        "code": "F",
        "nom": "Foie"
    }

]
```

### GET /api/json/autocomplete/symptoms?q=[query]

Effectue l'autocomplétion d'une recherche de symptôme.

+ ?q=douleur
+ Content-Type: application/json

```json
[

    {
        "idS": "40",
        "desc": "Chaleur et douleur à la paume des mains"
    },
    ...
]
```

## Développement

<div style="text-align: center">
    <img src="test/coverage.png" alt="project test coverage"></img>
</div>

## Installation de la base de données

Pour installer la base de données, on utilise un serveur mariadb
(Fork open source de mysql).

En tant que root, ou un utilisateur déjà enregistré on peut lancer la
commande suivante.

```
$ mysql -u $USER -p < src/Acupuncture/DB/acuBD.sql
```

## Installation des dépendances

Ce projet est basé sur composer

```
$ composer install
```

Pour générer l'autoloader

```
$ composer dump-autoload -o
```

## Lancement d'un serveur php

On peut utiliser un vrai serveur web tel que nginx, toutefois
pour le développement on peut utiliser le serveur embarqué avec PHP.

```
$ php -S localhost:8000
```

## Git Cmds
```
git status
git add -ip
git status
git add web/
git commit #Dynamique et on ajoute le message
git pull -r #On rebase
git log --graph --decorate --all #On affiche un graphique avec les différentes branches
git push origin master


# En cas de conflit
# On utilise VSCode on add puis
git rebase --continue
```

