<?php

namespace Acupuncture\Utils\tests\units;

use Acupuncture\Utils\Router;
use atoum;

/**
 * Class Route
 * @package Acupuncture\Utils\tests\units
 * @php 7.0
 * @extensions pdo
 */
class Route extends atoum {

    public function test_getRouteShouldBeMatchedByRoute() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);
        $routeStr = '/users/';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'users',
                'controller' => $routeController,
            )))
            ->when($accessedRoute = '/users/')
            ->then
            ->boolean($route->match($accessedRoute))
                ->isTrue();
    }

    public function test_newRouteShouldThrowIfNotAbstractRouteController() {
        $this
            ->exception(function () {
                new \Acupuncture\Utils\Route(array(
                    'controller' => null,
                    'name' => 'failure',
                    'url' => '/'
                ));
            })
            ->message
            ->contains('AbstractRouteController');
    }

    public function test_getRouteWithParamShouldMatchRoute() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/users/:username/:id';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'users',
                'controller' => $routeController,
            )))
            ->when($accessedRoute = '/users/john/3')
            ->then
            ->boolean($route->match($accessedRoute))
                ->isTrue();
    }

    public function test_routeWithoutParamShouldNotMatch() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/users/:username/:id';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'users',
                'controller' => $routeController,
            )))
            ->when($accessedRoute = '/users')
            ->then
            ->boolean($route->match($accessedRoute))
            ->isFalse();
    }

    public function test_routeIsRouteName() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/users';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'users',
                'controller' => $routeController,
            )))
            ->when($routeName = 'users')
            ->then
            ->boolean($route->is($routeName))
            ->isTrue();
    }

    public function test_routeUrlAccessor() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = 'users';
        $this
            ->when($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'users',
                'controller' => $routeController,
            )))
            ->string($route->url())
                ->isEqualTo($routeStr);
    }

    public function test_routeUrlAccessorShouldGiveTrimmedVersion() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/users';
        $this
            ->when($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'users',
                'controller' => $routeController,
            )))
            ->string($route->url())
                ->isEqualTo('users');
    }

    public function test_routeShouldRunControllerMethod() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/users/:username/:id';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'users',
                'controller' => $routeController,
            )))
            ->when($route->run(new \mock\Acupuncture\Utils\Router))
            ->mock($routeController)
                ->call('run')
                    ->once();
    }

    public function test_buildRouteShouldReturnRouteWithParamsAsUrl() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/posts/page/:page/:slug';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'posts',
                'controller' => $routeController,
            )))
            ->string($route->url(array('page' => 2, 'slug' => 'this-is-a-slug')))
                ->isEqualTo('posts/page/2/this-is-a-slug');
    }

    public function test_urlWithoutParameterShouldReturnTheUrl() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/posts';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'posts',
                'controller' => $routeController,
            )))
            ->string($route->url())
            ->isEqualTo('posts');
    }

    public function test_routeToString() {
        $this->mockGenerator()->shuntParentClassCalls();
        $mockedPDO = new \mock\PDO('mock');
        $routeController = new \mock\Acupuncture\Controllers\AbstractRouteController($mockedPDO);
        $routeStr = '/posts';
        $this
            ->given($route = new \Acupuncture\Utils\Route(array(
                'url' => $routeStr,
                'name' => 'posts',
                'controller' => $routeController,
            )))
            ->string($route->toString())
            ->isEqualTo("{ name: posts, url: posts, controller: " . get_class($routeController) . " }");
    }

}
