<?php

namespace Acupuncture\Utils\tests\units;
use atoum;

class Router extends atoum {

    public function test_createGetRouteShouldRunTheProvidedController() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->get(array(
                    'name' => 'users',
                    'url' => '/users',
                    'controller' => $controller,
                )))
                ->and($_SERVER['REQUEST_METHOD'] = 'GET')
                ->and($_GET['route'] = '/users')
            ->if($router->run())
            ->then
                ->mock($controller)
                    ->call('run')
                    ->once();
    }

    public function test_newRouterWithBaseUrlShouldBeTrimmed() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router(array('base_url' => '/')))
            ->then
            ->string($router->baseUrl())
                ->isEqualTo('');
    }

    public function test_createGetRouteWithParamsShouldRunTheProvidedController() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->get(array(
                    'name' => 'user',
                    'url' => '/users/:username',
                    'controller' => $controller
                )))
                ->and($_SERVER['REQUEST_METHOD'] = 'GET')
                ->and($_GET['route'] = '/users/john')
            ->if($router->run())
            ->then
                ->mock($controller)
                    ->call('run')
                        ->withArguments($router, array('username' => 'john'))
                    ->once();
    }

    public function test_createPostRouteWithoutParamsShouldRunTheController() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->post(array(
                    'url' => '/users',
                    'name' => 'newUser',
                    'controller' => $controller,
                )))
                ->and($_SERVER['REQUEST_METHOD'] = 'POST')
                ->and($_GET['route'] = '/users')
            ->if($router->run())
            ->then
                ->mock($controller)
                    ->call('run')
                    ->once();
    }

    public function test_createPostRouteWithParamsShouldRunTheController() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->post(array(
                    'url' => '/users/:username/:id',
                    'name' => 'newUser',
                    'controller' => $controller,
                )))
                ->and($_SERVER['REQUEST_METHOD'] = 'POST')
                ->and($_GET['route'] = '/users/john/3')
            ->if($router->run())
            ->then
                ->mock($controller)
                    ->call('run')
                        ->withArguments($router, array('username' => 'john', 'id' => 3))
                    ->once();
    }

    public function test_createGetRouteWithTwoParamsShouldRunTheProvidedController() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->get(array(
                    'name' => 'username',
                    'url' => '/users/:username/:id',
                    'controller' => $controller
                )))
                ->and($_SERVER['REQUEST_METHOD'] = 'GET')
                ->and($_GET['route'] = '/users/john/3')
            ->if($router->run())
            ->then
                ->mock($controller)
                    ->call('run')
                    ->withArguments($router, array('username' => 'john', 'id' => '3'))
                ->once();
    }

    public function test_createRouteWithNonAbstractRouteControllerShouldThrowException() {
        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->exception(function() use ($router) {
                $router->method('GET', array(
                    'url' => '/users',
                    'name' => 'users',
                    'controller' => null,
                ));
            })
            ->isInstanceOf('\Exception');
    }

    public function test_RouteWithNonHttpMethodShouldThrowException() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->exception(function() use ($router, $controller) {
                $router->method('NONHTTP', array(
                    'name' => 'fakeRoute',
                    'url' => 'fakeRoute',
                    'controller' => $controller,
                ));
            })
            ->isInstanceOf('\Exception');
    }

    public function test_getRouteToDefinedNameShouldReturnSpecificRoute() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->method('GET', array(
                        'name' => 'fake',
                        'url' => 'fake',
                        'controller' => $controller,
                )))
            ->then
                ->string($router->getRouteTo('fake'))
                ->isEqualTo('/?route=fake');
    }

    public function test_getRouteToDefinedNameShouldReturnUrlWithUrlRewriting() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->method('GET', array(
                    'name' => 'fake',
                    'url' => 'fake',
                    'controller' => $controller,
                )))
                ->and($_SERVER['URL_REWRITING'] = 'on')
            ->then
                ->string($router->getRouteTo('fake'))
                    ->isEqualTo('/fake');
    }

    public function test_getRouteToDefinedNameShouldThrowExceptionIfRouteDoesntExist() {
        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->then
                ->exception(function () use ($router) { $router->getRouteTo('fake'); })
                ->isInstanceOf('\Exception');
    }

    public function test_runRouteIfNoRouteProvidedShouldThrowException() {
        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->then
                ->exception(function () use ($router) { $router->run(); })
                    ->isInstanceOf('\Exception');
    }

    public function test_runRouteIfTheRouteDoesntExistShouldThrowException() {
        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($_GET['route'] = 'fake')
                ->and($_SERVER['REQUEST_METHOD'] = 'GET')
            ->then
            ->exception(function () use ($router) { $router->run(); })
            ->isInstanceOf('\Exception');
    }

    public function test_retrieveRouteFromRouteListWhenRouteDoesntExistShouldThrowException() {
        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->then
                ->exception(function () use ($router) { $router->route('fake'); })
                    ->isInstanceOf('\Exception');
    }

    public function test_redirectToNonExistingRouteShouldThrowException() {
        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->then
                ->exception(function () use ($router) { $router->redirectTo('fake'); })
                    ->isInstanceOf('\Exception');
    }

    public function test_redirectToExistingRouteShouldRedirectToUrl() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
                ->and($router->method('GET', array(
                    'name' => 'fake',
                    'url' => 'fake',
                    'controller' => $controller,
                )))
                ->and($router->redirectTo('fake'))
            ->then
                ->integer(http_response_code())
                    ->isEqualTo(302);
    }

    public function test_defaultRouteShouldDefineADefaultRoute() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->and($router->method('GET', array(
                'name' => 'fake',
                'url' => 'fake',
                'controller' => $controller,
            )))
            ->and($router->defaultRoute('fake'))
            ->string($router->defaultRoute())
                ->isEqualTo('fake');
    }

    public function test_defaultRouteWhenNotDefinedShouldThrowException() {
        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->exception(function () use ($router) { $router->defaultRoute('fake'); })
                ->isInstanceOf('\Exception');
    }

    public function test_toString() {
        $this->mockGenerator()->shuntParentClassCalls();
        $pdo = new \mock\PDO('mock');
        $controller = new \mock\Acupuncture\Controllers\AbstractRouteController($pdo);

        $this
            ->given($router = new \Acupuncture\Utils\Router())
            ->and($router->method('GET', array(
                'name' => 'fake',
                'url' => 'fake',
                'controller' => $controller,
            )))
            ->string($router->toString())
            ->isEqualTo("Router: {\n" .
                        "  GET: [\n" .
                        "    { name: fake, url: fake, controller: " . get_class($controller) . " }\n".
                        "  ]\n".
                        "}");
    }
}