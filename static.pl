#!/usr/bin/perl

use strict;
use warnings;
use utf8;

sub main {
    my ($filename, $search, $replace) = @_;

    $replace //= $search;

    my ($file, $ext) = $search =~ /^(.+)\.(.+)$/;

    my $re = qr{(href|src)\="(.+)${file}\.?.*\.${ext}\s*"};

    open (FILE, '<', $filename) || die "Impossible to open $filename \n";
    my @old_lines = ();
    while (<FILE>) {
        if ($_ =~ /$re/) {
            s/${re}/$1="$2${replace}"/g;
        }
       push(@old_lines, $_);
    }
    close (FILE) || die "Impossible to close $filename\n";

    print @old_lines;
    print "\n";
    open (FILE, '>', $filename) || die "Impossible to open $filename in >\n";
    print FILE @old_lines;
    close (FILE) || die "Impossible to close $filename in >\n"
}

main(@ARGV);

1;
__END__