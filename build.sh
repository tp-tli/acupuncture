#!/bin/bash


if [[ -z $1 || $1 = 'help' ]]; then
    help=$(cat << EOF
Usage: $0 COMMAND [arg...]

Commands:
    autoload     dump the autoload of composer using docker
    atoum        launch atoum tests
    composer     use composer inside a temporary docker container, use params as using composer
    help         prints this help
EOF
);
    echo "$help"
    exit 0
fi

if [[ "$UID" -ne 0 ]]; then
    echo 'use sudo!'
    exit 1
fi

if [[ $1 = 'autoload' ]]; then
    docker run --name composer --rm -v "$PWD":/app composer composer dump-autoload -o;
    exit $?
fi

if [[ $1 = 'composer' ]]; then
    # removing composer
    shift
    docker run --name composer --rm -v "$PWD":/app composer composer $@
    exit $?
fi

if [[ $1 = 'atoum' ]]; then
    ./vendor/bin/atoum -d test
    exit $?
fi