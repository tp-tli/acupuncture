/**
 * This file is an helper to autocomplete and display different elements in the search page
 */
(function () {
    var DISP = false;

    function option(value) {
        var node = document.createElement('option');
        node.value = value;
        return node;
    }

    function li(text, options) {
        var node = document.createElement('li');
        node.appendChild(document.createTextNode(text));
        if (options) Object.keys(options).forEach(function(key) { node[key] = options[key]; });
        return node;
    }

    function formatResults(json) {
        return json.reduce(function (html, current) {
            var value = current.desc || current.nom;
            html.appendChild(option(value));
            return html;
        }, document.createElement('div')).childNodes;
    }

    function appendChildren(parent, children) {
        for (var i = 0; i < children.length; i++) {
            parent.appendChild(children.item(i));
        }
    }

    function removeAllChildren(parent) {
        while (parent.firstChild) parent.removeChild(parent.firstChild);
    }

    function getJSON(url, resultSet, text) {
        var urlConcat = (url.indexOf('?') === -1) ? '?' : '&';
        var finalUrl = url + urlConcat + 'q=' + encodeURIComponent(text);
        return fetch(finalUrl)
            .then(function (resp) {
                return resp.json();
            })
            .then(function (responseJSON) {
                var datalist = document.getElementById(resultSet);
                removeAllChildren(datalist);
                appendChildren(datalist, formatResults(responseJSON));
            });
    }

    function executeSearch(event, url, resultSetId) {
        event.preventDefault();
        var text = event.target.value;
        if (text.length > 1 && event.keyCode !== 40 && event.keyCode !== 38) {
            getJSON(url, resultSetId, text);
        }
    }

    function appendToList(listId, text, className) {
        var list = document.getElementById(listId);
        if (list.classList.contains('hidden')) list.classList.remove('hidden');
        list.appendChild(
            li(text, { className: className })
        );
    }

    ['pathologies', 'meridiens', 'symptomes'].forEach(function (element) {
        var htmlElement = document.getElementById('list-' + element);
        htmlElement
            .addEventListener('keyup', function (e) {
                var autocompleteUrl = document.getElementById(
                    'autocomplete-' + element + '-url'
                ).value;
                executeSearch(e, autocompleteUrl, 'autocomplete-result-' + element);
            });
        var datalist = document.getElementById('autocomplete-result-' + element);
        if (!datalist.dataset.added) return;

        var appendToCurrentList = function (text) {
            appendToList.call(null, datalist.dataset.added, text, element + '-element');
        };

        htmlElement.addEventListener('input', function (e) {
            e.preventDefault();

            var listPathologies = document.getElementById('list-pathologies');
            if (listPathologies.validity.customError) {
                listPathologies.setCustomValidity('');
            }

            /* According to this post there is no other way !
             * http://stackoverflow.com/questions/30022728/perform-action-when-clicking-html5-datalist-option
             */
            for (var i = 0; i < datalist.childNodes.length; i++) {
                var node = datalist.childNodes.item(i);
                if (node.value === e.target.value) {
                    appendToCurrentList(e.target.value);
                    e.target.value = '';
                    return true;
                }
            }

            return false;
        });
    });

    /**
     * apply a map function on a NodeList
     * @param {NodeList} elementList
     * @param callback
     * @returns {Array}
     */
    function map(elementList, callback) {
        var results = [];
        for (var i = 0; i < elementList.length; i++) {
            results.push(callback(elementList[i], i));
        }
        return results;
    }

    function getSympAndMerQueryString() {
        var symptomsElements = document.getElementsByClassName('symptomes-element');
        var meridiansElements = document.getElementsByClassName('meridiens-element');

        var symptoms = map(symptomsElements, function (e) {
                return e.textContent;
            })
            .concat(document.getElementById('list-symptomes').value)
            .filter(function (e) { return e !== undefined && e !== ''; })
            .join(',');

        var meridians = map(meridiansElements, function (e) {
                return e.textContent;
            })
            .concat(document.getElementById('list-meridiens').value)
            .filter(function (e) { return e !== undefined && e !== ''; })
            .join(',');

        return { symp: symptoms, mer: meridians };
    }

    function obj2QueryString(obj) {
        return Object.keys(obj)
            .map(function (k) {
                if (obj[k]) return k + '=' + encodeURIComponent(obj[k]);
                return undefined;
            })
            .filter(function (e) { return e !== undefined; })
            .join('&');
    }

    function qs(url, obj) {
        var urlConcat = url.indexOf('?') === -1
                        ? '?'
                        : '&';
        return url + urlConcat + obj2QueryString(obj);
    }

    function formIsEmpty() {
        return !(document.getElementById('list-pathologies').value !== '' ||
                document.getElementById('list-meridiens').value !== '' ||
                document.getElementById('list-symptomes').value !== '' ||
                document.getElementById('added-meridiens').childElementCount !== 0 ||
                document.getElementById('added-symptomes').childElementCount !== 0);
    }

    document.getElementById('search-form')
            .addEventListener('submit', function (e) {
                e.preventDefault();
                var listPathologies = document.getElementById('list-pathologies');

                var pathologyName = listPathologies.value;
                var formUrl = e.currentTarget.action;
                var obj = getSympAndMerQueryString();
                obj['name'] = pathologyName;

                var url = qs(formUrl, obj);

                if (formIsEmpty()) {
                    listPathologies.setCustomValidity('Veuillez compléter ce champ.');
                } else {
                    window.location = url;
                }

                return false;
            });

    document.getElementById('add-more-filters')
            .addEventListener('click', function (e) {
                if (DISP === false) {
                    document.getElementById('filter-list').style.display = 'block';
                    document.getElementById('list-pathologies').required = false;
                    DISP = true;
                    e.target.value = '- de critères';
                } else {
                    document.getElementById('filter-list').style.display = 'none';
                    document.getElementById('list-pathologies').required = true;
                    DISP = false;
                    e.target.value = '+ de critères';
                }
            });
})();
