<?php

use Acupuncture\Controllers\AboutController;
use Acupuncture\Controllers\AuthenticationUserController;
use Acupuncture\Controllers\CreateUserController;
use Acupuncture\Controllers\DisplayPathologyController;
use Acupuncture\Controllers\IndexController;
use Acupuncture\Controllers\JSON\AutocompleteController;
use Acupuncture\Controllers\JSON\JSONListSymptomsController;
use Acupuncture\Controllers\ListPathologiesController;
use Acupuncture\Controllers\ListSymptomsController;
use Acupuncture\Controllers\SearchController;
use Acupuncture\Controllers\ValidateUserController;
use Acupuncture\Controllers\DisconnectController;
use Acupuncture\Controllers\ForbiddenController;
use Acupuncture\Controllers\XML\XMLListSymptomsController;
use Acupuncture\Utils\Router;

function configRouter(Router $router, PDO $db) {
    $router->get(array(
        'url' => 'home',
        'controller' => new IndexController($db),
        'name' => 'get_index'
    ));

    $router->get(array(
        'url' => 'error',
        'controller' => new ForbiddenController($db),
        'name' => 'get_error'
    ));

    $router->get(array(
        'url' => 'about',
        'controller' => new AboutController($db),
        'name' => 'get_about'
    ));

    $router->get(array(
        'url' => 'create-user',
        'controller' => new CreateUserController($db),
        'name' => 'get_create-user'
    ));

    $router->get(array(
        'url' => 'disconnect',
        'controller' => new DisconnectController($db),
        'name' => 'get_disconnect'
    ));

    $router->post(array(
        'url' => 'create-user',
        'controller' => new ValidateUserController($db),
        'name' => 'post_create-user'
    ));

    $router->get(array(
        'url' => 'pathologies',
        'controller' => new ListPathologiesController($db),
        'name' => 'get_pathologies'
    ));

    $router->get(array(
        'url' => 'symptoms-and-pathologies',
        'controller' => new ListSymptomsController($db),
        'name' => 'get_symptoms-and-pathologies'
    ));

    $router->get(array(
        'url' => 'symptoms-and-pathologies/page/:page',
        'controller' => new ListSymptomsController($db),
        'name' => 'get_paged_symptoms-and-pathologies'
    ));

    $router->get(array(
        'url' => 'search',
        'controller' => new SearchController($db),
        'name' => 'get_search'
    ));

    $router->get(array(
        'url' => 'api/json/autocomplete/pathologies',
        'controller' => new AutocompleteController($db, array('model' => \Acupuncture\Models\Pathology::class)),
        'name' => 'get_autocomplete_pathologies'
    ));

    $router->get(array(
        'url' => 'api/json/autocomplete/symptoms',
        'controller' => new AutocompleteController($db, array('model' => \Acupuncture\Models\Symptom::class)),
        'name' => 'get_autocomplete_symptoms'
    ));

    $router->get(array(
        'url' => 'api/json/autocomplete/meridians',
        'controller' => new AutocompleteController($db, array('model' => \Acupuncture\Models\Meridian::class)),
        'name' => 'get_autocomplete_meridians'
    ));

    $router->post(array(
        'url' => 'login',
        'controller' => new AuthenticationUserController($db),
        'name' => 'post_authenticate-user'
    ));

    $router->get(array(
        'url' => 'api/xml/pathologies',
        'controller' => new XMLListSymptomsController($db),
        'name' => 'get_api_xml_pathologies',
    ));

    $router->get(array(
        'url' => 'api/xml/pathologies/page/:page',
        'controller' => new XMLListSymptomsController($db),
        'name' => 'get_api_xml_pathologies',
    ));

    $router->get(array(
        'url' => 'api/json/pathologies',
        'controller' => new JSONListSymptomsController($db),
        'name' => 'get_api_json_pathologies',
    ));

    $router->get(array(
        'url' => 'api/json/pathologies/page/:page',
        'controller' => new JSONListSymptomsController($db),
        'name' => 'get_api_json_pathologies',
    ));


    $router->get(array(
        'url' => 'display',
        'controller' => new DisplayPathologyController($db),
        'name' => 'get_display-pathology',
    ));

    $router->defaultRoute('get_index');
    \Acupuncture\Config\TwigConfig::configTwig($router);
}