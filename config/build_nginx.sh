#!/bin/bash

file="./nginx.php.conf"
if [[ -s "$1" ]]; then
    file="$1"
fi

export ROOT="/usr/share/nginx/php" # No / at the end of ROOT

sed -e "s#\${ROOT}#${ROOT}#g" "$file" > "php.conf"
