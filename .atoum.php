<?php
use mageekguy\atoum;

$report = $script->addDefaultReport();
$coverageHtmlFile = new atoum\report\fields\runner\coverage\html('Acupuncture', __DIR__. '/test/coverage');
$coverageHtmlFile->setRootUrl('http://local.host/acupuncture/coverage');
$report->addField(
    $coverageHtmlFile
);