#!/bin/bash

function md5_and_link {
    md5=$(md5sum "$1" | cut -d' ' -f1)
    ext=${1##*.}
    new_file_name=${1%.$ext}.${md5}.${ext}
    filename_only=$(basename "$1")
    ln -vs "$filename_only" "$new_file_name"
    new_filename_only=$(basename "$new_file_name");

    basename "$new_file_name" >> "$(dirname "$1")"/.gitignore

    rename_in_files "$1" "$new_filename_only";
}

function rename_in_files {
    filename="$1"
    extension=${1##*.}
    filename_only=$(basename "$1")
    filename_without_extension=${filename_only%.$extension};

    files=$(grep -P '(href|src).+'${filename_without_extension}'.*\.'${extension} views/*html | cut -d: -f1)
    if [[ ! -z "$2" ]]; then
        for f in $files; do
            perl static.pl "$f" "$filename_only" "$2"
        done
    else
        for f in $files; do
            perl static.pl "$f" "$filename_only"
        done
    fi
}

for type in css js; do
    DIR=web/"$type"
    /bin/rm -f "$DIR"/.gitignore
    find "$DIR" -type l -exec /bin/rm {} +
    for f in "$DIR"/*."$type"; do
        if [[ $1 = 'dev' ]]; then
            rename_in_files "$f"
        else
            md5_and_link "$f"
        fi
    done
done
